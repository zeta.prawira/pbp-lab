import 'package:flutter/material.dart';

class MyApp extends StatefulWidget {
  @override
  LayananIsolasi createState() => LayananIsolasi();
}

class LayananIsolasi extends State<MyApp> {
  TextEditingController namaLengkap = TextEditingController();
  TextEditingController umur = TextEditingController();
  TextEditingController kotaKabupaten = TextEditingController();
  TextEditingController namaIbuKandung = TextEditingController();

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          bottomOpacity: 0.0,
          elevation: 0.0, //(0xff222831),
          title: Text(
            "Layanan Isolasi",
            style: TextStyle(
              color: Colors.black,
            ),
          ),
          iconTheme: IconThemeData(color: Colors.black),
        ),
        drawer: Drawer(
            child: Container(
          color: Color(0xff222831),
          child: ListView(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                          padding: EdgeInsets.only(left: 18, top: 30),
                          child: Text(
                            "Selamat siang,",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                                color: Colors.white),
                          )),
                      Padding(
                        padding: EdgeInsets.only(left: 18, top: 3),
                        child: Text(
                          "Hai Zeta!👋",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                      padding: EdgeInsets.only(right: 35, top: 35),
                      child: ElevatedButton(
                        onPressed: () {
                          print("Raised Button");
                        },
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.red[900])),
                        child: Text(
                          "Log Out",
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                ],
              ),
              Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: ListTile(
                    title: const Text(
                      'Vaksinasi COVID-19',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    onTap: () {
                      // Update the state of the app
                      // ...
                      // Then close the drawer
                      Navigator.pop(context);
                    },
                  )),
              ListTile(
                title: const Text('Layanan Isolasi Mandiri',
                    style: TextStyle(
                      color: Colors.white,
                    )),
                onTap: () {
                  // Update the state of the app
                  // ...
                  // Then close the drawer
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text('Rumah Sakit Rujukan',
                    style: TextStyle(
                      color: Colors.white,
                    )),
                onTap: () {
                  // Update the state of the app
                  // ...
                  // Then close the drawer
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text('Tim Pakar COVID-19',
                    style: TextStyle(
                      color: Colors.white,
                    )),
                onTap: () {
                  // Update the state of the app
                  // ...
                  // Then close the drawer
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text('Kontak Layanan',
                    style: TextStyle(
                      color: Colors.white,
                    )),
                onTap: () {
                  // Update the state of the app
                  // ...
                  // Then close the drawer
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text('Persebaran Data',
                    style: TextStyle(
                      color: Colors.white,
                    )),
                onTap: () {
                  // Update the state of the app
                  // ...
                  // Then close the drawer
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text('Edukasi Protokol',
                    style: TextStyle(
                      color: Colors.white,
                    )),
                onTap: () {
                  // Update the state of the app
                  // ...
                  // Then close the drawer
                  Navigator.pop(context);
                },
              ),
              Padding(
                  padding: EdgeInsets.only(left: 15, top: 20),
                  child: Text(
                    "'Marilah kita lakukan vaksinasi agar Indonesia terbebas dari pandemi' -Infid",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 24,
                        fontStyle: FontStyle.italic,
                        color: Colors.white),
                  )),
              Image.asset("images/CovidIcon.png")
            ],
          ),
        )),
        body: SingleChildScrollView(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            Text(
              "Panduan Penggunaan Layanan Isolasi Mandiri COVID-19",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 14,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8, left: 5, right: 5),
              child: Text(
                "Alur dalam penggunaan layanan isolasi mandiri ini diawasi\noleh Kementerian Kesehatan Republik Indonesia",
                style: TextStyle(
                  fontSize: 12,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 19),
                child: Column(
                  children: [
                    Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      clipBehavior: Clip.antiAlias,
                      color: Color(0xff00ADB5),
                      elevation: 20,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 10, bottom: 10, right: 8, left: 8),
                            child: Column(
                              children: [
                                Text(
                                  "1",
                                  style: TextStyle(color: Colors.pink[50]),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 16, bottom: 8),
                                  child: Text(
                                    "Melakukan Tes PCR",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                Text(
                                  "Pasien melakukan tes PCR / swab antigen di laboratorium yang terafiliasi dengan Kementerian Kesehatan RI. Jika hasil tesnya positif dan lab melaporkan hasilnya ke database kasus positif COVID-19 di Kemenkes (NAR), maka pasien akan menerima Whatsapp dari Kemenkes RI (dengan centang hijau) secara otomatis.",
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      clipBehavior: Clip.antiAlias,
                      color: Color(0xff00ADB5),
                      elevation: 20,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 10, bottom: 10, right: 8, left: 8),
                            child: Column(
                              children: [
                                Text(
                                  "2",
                                  style: TextStyle(color: Colors.pink[50]),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 16, bottom: 8),
                                  child: Text(
                                    "Konsultasi dengan Dokter",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                Text(
                                  "Lakukan konsultasi dokter dengan menginformasikan Anda adalah pasien program Kementerian Kesehatan.",
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      clipBehavior: Clip.antiAlias,
                      color: Color(0xff00ADB5),
                      elevation: 20,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 10, bottom: 10, right: 8, left: 8),
                            child: Column(
                              children: [
                                Text(
                                  "3",
                                  style: TextStyle(color: Colors.pink[50]),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 16, bottom: 8),
                                  child: Text(
                                    "Resep Digital",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                Text(
                                  "Setelah melakukan konsultasi secara daring, dokter akan memberikan resep digital sesuai dengan kondisi pasien. Jika pasien masuk dalam kategori yang dapat melakukan isoman, obat dapat ditebus gratis.",
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      clipBehavior: Clip.antiAlias,
                      color: Color(0xff00ADB5),
                      elevation: 20,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 10, bottom: 10, right: 8, left: 8),
                            child: Column(
                              children: [
                                Text(
                                  "4",
                                  style: TextStyle(color: Colors.pink[50]),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 16, bottom: 8),
                                  child: Text(
                                    "Pengiriman Obat",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                Text(
                                  "Kementerian Kesehatan bekerja sama dengan jasa pengiriman dari SiCepat untuk mengambil obat dan/atau vitamin dari Apotek Kimia Farma dan mengirimkan ke alamat pasien.",
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                          side: BorderSide(color: Colors.black)),
                      clipBehavior: Clip.antiAlias,
                      color: Color(0xff00ADB5),
                      elevation: 20,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 10, bottom: 10, right: 8, left: 8),
                            child: Column(
                              children: [
                                Text(
                                  "5",
                                  style: TextStyle(color: Colors.pink[50]),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 16, bottom: 8),
                                  child: Text(
                                    "Mengisi Formulir Isolasi Mandiri",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                Text(
                                  "Anda yang akan melakukan isolasi mandiri harap mengisi formulir dibawah ini agar biodata Anda tersimpan oleh Kementerian Kesehatan dan tetap terkontrol oleh Dokter.",
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8),
                      child: Icon(
                        Icons.arrow_circle_down,
                        color: Colors.black,
                        size: 30,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 0, bottom: 8),
                      child: Text(
                        "Formulir Melakukan Isolasi Mandiri",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16),
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 6, horizontal: 10),
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: 35,
                            margin: EdgeInsets.only(bottom: 8),
                            child: TextField(
                              controller: namaLengkap,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'Nama Lengkap',
                                isDense: true,
                              ),
                            ),
                          ),
                          Container(
                            height: 35,
                            margin: EdgeInsets.only(bottom: 8),
                            child: TextField(
                              controller: umur,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'Umur',
                                isDense: true,
                              ),
                            ),
                          ),
                          Container(
                            height: 35,
                            margin: EdgeInsets.only(bottom: 8),
                            child: TextField(
                              controller: kotaKabupaten,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'Kota/Kabupaten',
                                isDense: true,
                              ),
                            ),
                          ),
                          Container(
                            height: 35,
                            child: TextField(
                              controller: namaIbuKandung,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'Nama Ibu Kandung',
                                isDense: true,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: ElevatedButton(
                              onPressed: () {
                                print(namaLengkap.text);
                                print(umur.text);
                                print(kotaKabupaten.text);
                                print(namaIbuKandung.text);
                              },
                              child: Text("Submit"),
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                      Color(0xff00ADB5))),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 14.0, bottom: 5),
                            child: Text(
                              "Konsultasi Daring",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                          ),
                          Wrap(
                            alignment: WrapAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 8),
                                child: Image.asset(
                                  "images/halodoc-logo.png",
                                  height: 17,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 8),
                                child: Image.asset(
                                  "images/klikdokter.jpeg",
                                  height: 20,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 8),
                                child: Image.asset(
                                  "images/gooddoctor.jpeg",
                                  height: 20,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 8),
                                child: Image.asset(
                                  "images/yesdok.jpeg",
                                  height: 20,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 8),
                                child: Image.asset(
                                  "images/sehatq.jpeg",
                                  height: 20,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 8),
                                child: Image.asset(
                                  "images/grabhealth.jpeg",
                                  height: 20,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 8),
                                child: Image.asset(
                                  "images/alodokter.jpeg",
                                  height: 20,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 8),
                                child: Image.asset(
                                  "images/klinikgo.jpeg",
                                  height: 20,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 8),
                                child: Image.asset(
                                  "images/milvik.jpeg",
                                  height: 20,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 8),
                                child: Image.asset(
                                  "images/getwell.jpeg",
                                  height: 20,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 8),
                                child: Image.asset(
                                  "images/trustmedis.png",
                                  height: 20,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 8),
                                child: Image.asset(
                                  "images/prosehat.jpeg",
                                  height: 20,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                )),
          ]),
        ));
  }
}

from django.urls import path
from .views import add_friend, index

urlpatterns = [
    path('', index),
    path('add', add_friend)
]

from datetime import date
from django.db import models
from django.db.models.fields import DateTimeField

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    name = models.CharField(max_length=30, default="")
    # TODO Implement missing attributes in Friend model
    npm = models.CharField(max_length=10, default="")
    dob = models.DateField(default=date.today)
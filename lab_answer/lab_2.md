1. Apakah perbedaan antara JSON dan XML?
Jawaban:

Kelebihan JSON dibanding XML:
- Syntax mudah.
- Semua JavaScript frameworks didukung JSON.
- Sebagian besar backend saat ini didukung JSON.
- Transmisi data struktur menggunakan koneksi jaringan.
- Support oleh semua browser.

Kelebihan XML dibanding JSON:
- Data dipisahkan dari HTML.
- Perubahan pada platform dapat lebih sederhana.
- Pertukaran data antar platform dapat lebih cepat.

Kekurangan JSON dibanding XML:
- Tool terbatas.

Kekurangan XML dibanding JSON:
- Syntax XML sulit dipahami.
- Membutuhkan tambahan aplikasi pemrosesan.

2. Apakah perbedaan antara HTML dan XML?
Jawaban:

Kelebihan HTML dibanding XML:
- UI browser HTML mudah dibuat.
- Syntax mudah dipahami.
- dll.

Kelebihan XML dibanding HTML:
- Data dari HTML berpisah.
- Perubahan pada platform dapat lebih sederhana.
- Pertukaran data antar platform dapat lebih cepat.

Kekurangan HTML dibanding XML:
- Tidak cocok untuk pertukaran data
- Tidak berorientasi objek.
- Tidak memungkinkan untuk penyimpanan dan pertukaran data

Kekurangan XML dibanding HTML:
- Syntax yang rumit.
- Butuh aplikasi pemrosesan.
- Tidak dapat membuat tag.
- dll.
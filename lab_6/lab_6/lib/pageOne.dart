import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class FirstPage extends StatelessWidget {
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Color(0xff222831), Color(0xff6f42c1)])),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            bottomOpacity: 0.0,
            elevation: 0.0, //(0xff222831),
            title: Text("Infid"),
          ),
          drawer: Drawer(
              child: Container(
            color: Color(0xff222831),
            child: ListView(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                            padding: EdgeInsets.only(left: 18, top: 30),
                            child: Text(
                              "Selamat siang,",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  color: Colors.white),
                            )),
                        Padding(
                          padding: EdgeInsets.only(left: 18, top: 3),
                          child: Text(
                            "Hai Zeta!👋",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                        padding: EdgeInsets.only(right: 35, top: 35),
                        child: RaisedButton(
                          onPressed: () {
                            print("Raised Button");
                          },
                          color: Colors.grey,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5)),
                          child: Text(
                            "Log Out",
                            style: TextStyle(color: Color(0xff222831)),
                          ),
                        )),
                  ],
                ),
                Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: ListTile(
                      title: const Text(
                        'Vaksinasi COVID-19',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      onTap: () {
                        // Update the state of the app
                        // ...
                        // Then close the drawer
                        Navigator.pop(context);
                      },
                    )),
                ListTile(
                  title: const Text('Layanan Isolasi Mandiri',
                      style: TextStyle(
                        color: Colors.white,
                      )),
                  onTap: () {
                    // Update the state of the app
                    // ...
                    // Then close the drawer
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  title: const Text('Rumah Sakit Rujukan',
                      style: TextStyle(
                        color: Colors.white,
                      )),
                  onTap: () {
                    // Update the state of the app
                    // ...
                    // Then close the drawer
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  title: const Text('Tim Pakar COVID-19',
                      style: TextStyle(
                        color: Colors.white,
                      )),
                  onTap: () {
                    // Update the state of the app
                    // ...
                    // Then close the drawer
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  title: const Text('Kontak Layanan',
                      style: TextStyle(
                        color: Colors.white,
                      )),
                  onTap: () {
                    // Update the state of the app
                    // ...
                    // Then close the drawer
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  title: const Text('Persebaran Data',
                      style: TextStyle(
                        color: Colors.white,
                      )),
                  onTap: () {
                    // Update the state of the app
                    // ...
                    // Then close the drawer
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  title: const Text('Edukasi Protokol',
                      style: TextStyle(
                        color: Colors.white,
                      )),
                  onTap: () {
                    // Update the state of the app
                    // ...
                    // Then close the drawer
                    Navigator.pop(context);
                  },
                ),
                Padding(
                    padding: EdgeInsets.only(left: 15, top: 20),
                    child: Text(
                      "'Marilah kita lakukan vaksinasi agar Indonesia terbebas dari pandemi' -Infid",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 24,
                          fontStyle: FontStyle.italic,
                          color: Colors.white),
                    )),
                Image.asset("images/CovidIcon.png")
              ],
            ),
          )),
          body: SingleChildScrollView(
            child: Center(
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: <Widget>[
                    Text(
                      "Selamat Datang di Infid!",
                      style: TextStyle(
                        fontSize: 22,
                        //fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 5),
                      child: Text(
                        "Info COVID-19 di Indonesia",
                        style: TextStyle(
                          fontSize: 13,
                          //fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10)),
                        padding: EdgeInsets.only(top: 50),
                        child: CarouselSlider(
                          options: CarouselOptions(
                            height: 300,
                            aspectRatio: 16 / 9,
                            viewportFraction: 0.8,
                            initialPage: 0,
                            enableInfiniteScroll: true,
                            reverse: false,
                            autoPlay: true,
                            autoPlayInterval: Duration(seconds: 3),
                            autoPlayAnimationDuration:
                                Duration(milliseconds: 800),
                            //autoPlayCurve: Curves.fastOutSlowIn,
                            //enlargeCenterPage: true,
                            //onPageChanged: callbackFunction,
                            scrollDirection: Axis.horizontal,
                          ),
                          items: [
                            'images/Covid1.png',
                            'images/Covid2.png',
                            'images/Covid3.png',
                          ].map((i) {
                            return Builder(
                              builder: (BuildContext context) {
                                return Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: EdgeInsets.symmetric(horizontal: 0),
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        children: [
                                          Image.asset(
                                            '$i',
                                            height: 300,
                                            width: 280,
                                          ),
                                        ]));
                              },
                            );
                          }).toList(),
                        )),
                    Padding(
                      padding: EdgeInsets.only(top: 50),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          TextButton(
                            style: TextButton.styleFrom(
                              primary: Color(0xff00ADB5), // foreground
                            ),
                            onPressed: () {},
                            child: Text(
                              'Sign Up',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 8),
                            child: RaisedButton(
                              onPressed: () {
                                print("Raised Button");
                              },
                              color: Color(0xff00ADB5),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                              child: Text(
                                "Log In",
                                style: TextStyle(color: Colors.black),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: Text(
                        "Silahkan Anda Log In untuk mengetahui informasi mengenai COVID-19 selengkapnya",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white, fontSize: 12),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 40, bottom: 15),
                      child: Text(
                        "Aplikasi ini telah divalidasi oleh:",
                        style: TextStyle(color: Colors.white, fontSize: 10),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 25),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Image.asset(
                                'images/kemenkes.png',
                                height: 30,
                              ),
                              Image.asset(
                                'images/kominfo.png',
                                height: 30,
                              ),
                              Image.asset(
                                'images/Kantor_Staf_Presiden.png',
                                height: 30,
                              ),
                              ClipRRect(
                                  borderRadius: BorderRadius.circular(5),
                                  child: Image.asset(
                                    'images/logo-bumn-vector-terbaru.jpg',
                                    height: 30,
                                  )),
                              Image.asset(
                                'images/polri.png',
                                height: 30,
                              ),
                            ])),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
